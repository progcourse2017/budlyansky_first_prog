#include "stdafx.h"
#include <iostream>

using namespace std;

struct time 
{
	unsigned hours;
	unsigned minutes;
	unsigned seconds;
	unsigned allInSec=0;
};


void showTime(time youTime);
time calcTime(time youTime);


int main(int argc, char *argv[])
{
	time comparisonTime[2];
	time resultTime;

	for (int i = 0; i < 2; i++)
	{
		cout << "\nEnter the " << i + 1 << " time:";

		while (comparisonTime[i].hours > 23) 
		{
			cout << "\nhours(max.23): ";
			cin >> comparisonTime[i].hours;
		}

		while (comparisonTime[i].minutes > 60)
		{
			cout << "\nminutes(max.60): ";
			cin >> comparisonTime[i].minutes;
			if (comparisonTime[i].minutes == 60)
			{
				comparisonTime[i].hours++;
				comparisonTime[i].minutes = 0;
			}

	    }

		while (comparisonTime[i].seconds > 60) 
		{
			cout << "\nseconds(max.60): ";
			cin >> comparisonTime[i].seconds;

			if (comparisonTime[i].seconds == 60)
			{
				comparisonTime[i].minutes++;
				comparisonTime[i].seconds = 0;
			}
		}
		
		comparisonTime[i].allInSec = comparisonTime[i].allInSec + comparisonTime[i].seconds + comparisonTime[i].minutes * 60 + comparisonTime[i].hours * 3600;
		cout << comparisonTime[i].allInSec;
	};
	
	if(comparisonTime[1].allInSec > comparisonTime[0].allInSec)
		resultTime.allInSec = comparisonTime[1].allInSec - comparisonTime[0].allInSec;
	else
		resultTime.allInSec = comparisonTime[0].allInSec - comparisonTime[1].allInSec;


	resultTime = calcTime(resultTime);

	cout << "\nRESULT:";
	showTime(resultTime);

	system("pause");

    return 0;
}

void showTime(time youTime)
{
	cout << "\n\n" << "\nhours - " << youTime.hours << "\n"
		 << "\nminutes - " << youTime.minutes << "\n"
		 << "\nseconds - " << youTime.seconds << "\n" << endl;

}

time calcTime(time youTime) 
{
	youTime.hours = youTime.allInSec / 3600;
	youTime.allInSec -= youTime.hours * 3600;

	youTime.minutes = youTime.allInSec / 60;
	youTime.allInSec -= youTime.minutes * 60;

	youTime.seconds = youTime.allInSec;

	return youTime;
}